<?php
  include ('head.php');
  include ('conexion.php');

  $resultado = mysqli_query($conexion, "SELECT * FROM films WHERE codigo = '" . $_REQUEST['codigo'] . "'");
$res = mysqli_fetch_array($resultado)
?>
<form action="actualizar2.php" method="post">

<form>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="Titulo-Pelicula">Título</label>
      <input type="text" class="form-control" name="titulonuevo" value="<?php echo $res['titulo'] ?>">
    </div>
    <div class="form-group col-md-6">
      <label for="Director-Pelicula">Director</label>
      <input type="text" class="form-control" name="directornuevo" value="<?php echo $res['director'] ?>">
    </div>
  </div>
  <div class="form-row">
  <div class="form-group col-md-3">
      <label for="Genero-Pelicula">Género</label>
      <select name="generonuevo" class="form-control">
        <option selected>Elige un género...</option>
        <option>Acción</option>
        <option>Aventuras</option>
        <option>Ciencia Ficción</option>
        <option>Comedia</option>
        <option>Documental</option>
        <option>Drama</option>
        <option>Fantasía</option>
        <option>Musical</option>
        <option>Suspense</option>
        <option>Terror</option>
      </select>
    </div>
    <div class="form-group col-md-2">
      <label for="Pais-Pelicula">País</label>
      <input type="text" name="paisnuevo" class="form-control" value="<?php echo $res['pais'] ?>">
    </div>
    <div class="form-group col-md-2">
      <label for="Año-Pelicula">Año</label>
      <input type="number" name="anhonuevo" class="form-control" value="<?php echo $res['anho'] ?>">
    </div>
    <div class="form-group col-md-2">
      <label for="Duracion-Pelicula">Duración</label>
      <input type="number" name="duracionnuevo" class="form-control" value="<?php echo $res['duracion'] ?>">
    </div>
    <div class="form-group col-md-2">
      <label for="Precio-Pelicula">Precio</label>
      <input type="number" class="form-control" value="<?php echo $res['precio'] ?>">
    </div>
  </div>

  <button type="submit" class="btn btn-primary">Aplicar Cambios</button>
</form>

</form>

