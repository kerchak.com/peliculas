
  <?php
  include ('head.php');
  include ('conexion.php');

  ?>

    <table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Título</th>
      <th scope="col">Director</th>
      <th scope="col">Género</th>
      <th scope="col">Año</th>
      <th scope="col">Duración</th>
      <th scope="col">País</th>
      <th scope="col">Precio</th>
      <th scope="col">Edición</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $query = 'SELECT * FROM films';
    $resultado = mysqli_query($conexion,$query) or die ("No he podido conectarme a la base de datos");

    while ($datos = mysqli_fetch_array($resultado)) {
        echo '<tr>';
        echo '<td>'. $datos['codigo'].'</td>';
        echo '<td>'. $datos['titulo'].'</td>';
        echo '<td>'. $datos['director'].'</td>';
        echo '<td>'. $datos['genero'].'</td>';
        echo '<td>'. $datos['anho'].'</td>';
        echo '<td>'. $datos['duracion'].'</td>';
        echo '<td>'. $datos['pais'].'</td>';
        echo '<td>'. $datos['precio'].'</td>';
        echo '<td>';
        echo '<a href="borrar.php?codigo='. $datos['codigo'] .'">Borrar</a>';
        echo '</td>';

    }
    ?>
    </tbody>
</table>
<?php
include ("footer.php");
?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>